<?php

class TabbedBlock
{

    private $items = [];
    private $id_counter = [];

    public static function init()
    {
        $class = __CLASS__;
        new $class;
    }

    function __construct()
    {
        $asset_file = include(plugin_dir_path(__FILE__) . 'build/index.asset.php');

        wp_register_script(
            'custom-blocks-tabbed-block-script',
            path_join(get_template_directory_uri(), 'inc/tabbed/build/index.js'),
            $asset_file['dependencies'],
            $asset_file['version']
        );

        wp_register_script(
            'custom-blocks-tabbed-block-frontend-script',
            path_join(get_template_directory_uri(), 'inc/tabbed/dist/frontend.js'),
            [],
            filemtime(path_join(get_template_directory(), 'inc/tabbed/dist/frontend.js'))
        );


        wp_register_style(
            'custom-blocks-tabbed-block-frontend-style',
            path_join(get_template_directory_uri(), 'inc/tabbed/dist/frontend.css'),
            [],
            filemtime(path_join(get_template_directory(), 'inc/tabbed/dist/frontend.css'))
        );


        register_block_type('custom-blocks/tabbed-block', array(
            'apiVersion' => 2,
            'editor_script' => 'custom-blocks-tabbed-block-script',
            'render_callback' => function ($block_attributes, $content) {
                return $this->block_render_callback($block_attributes, $content);
            },
            'script' => 'custom-blocks-tabbed-block-frontend-script',
            'style' => 'custom-blocks-tabbed-block-frontend-style'
        ));

        register_block_type('custom-blocks/tabbed-block-child', array(
            'apiVersion' => 2,
            'editor_script' => 'custom-blocks-tabbed-block-script',
            'render_callback' => function ($block_attributes, $content) {
                return $this->block_child_render_callback($block_attributes, $content);
            }
        ));
    }

    private function block_child_render_callback($block_attributes, $content)
    {
        $this->items[] = [
            "attributes" => $block_attributes,
            "content" => $content
        ];
    }

    private function block_render_callback($block_attributes, $content)
    {

        $data = [
            "titles" => ["<div role='tablist' class='tab-controls'>"],
            "contents" => ["<div class='tab-panels'>"]
        ];


        foreach ($this->items as $index => $block_item) {

            $tab_name = join("_", explode(" ", strtolower($block_item["attributes"]["title"])));

            if (array_key_exists($tab_name, $this->id_counter)) {
                $this->id_counter[$tab_name] += 1;
            } else {
                $this->id_counter[$tab_name] = 1;
            }

            $id_postfix = $this->id_counter[$tab_name] > 1 ? $this->id_counter[$tab_name] : "";

            $tab_attributes = array(
                "class" => "tabbed-item-control" . ($index === 0 ? " active" : ""),
                "role" => "tab",
                "aria-selected" => $index === 0 ? "true" : "false",
                "aria-controls" => "tab-panel-" . $tab_name . $id_postfix,
                "id" => "tab-control-" . $tab_name . $id_postfix,
                "tabindex" => $index === 0 ? "0" : "-1"
            );

            $panel_attributes = array(
                "id" => "tab-panel-" . $tab_name . $id_postfix,
                "role" => "tabpanel",
                "tabindex" => "0",
                "aria-labelledby" => "tab-control-" . $tab_name . $id_postfix,
                "hidden" => $index !== 0,
                "class" => "tabbed-item-panel",
            );

            $data["titles"][] = "<button " . $this->make_attributes_from_array($tab_attributes) . ">" . $block_item["attributes"]["title"] . "</button>";
            $data["contents"][] = "<div " . $this->make_attributes_from_array($panel_attributes) . ">" . $block_item["content"] . "</div>";
        }

        $data["titles"][] = "</div>";
        $data["contents"][] = "</div>";

        $this->items = [];

        return "<div class='tabbed-block-container'>" . join("", $data["titles"]) . join("", $data["contents"]) . "</div>";
    }

    private function make_attributes_from_array($arr)
    {
        $attrs = [];
        foreach ($arr as $key => $value) {
            if (gettype($value) === "boolean") {
                if ($value) $attrs[] = $key;
            } else {
                $attrs[] = $key . "=" . '"' . $value . '"';
            }
        }
        return join(" ", $attrs);
    }
}
