window.addEventListener("DOMContentLoaded", function () {
  var tabs = document.querySelectorAll('[role="tab"]');
  var tabList = document.querySelector('[role="tablist"]'); // Add a click event handler to each tab

  tabs.forEach(function (tab) {
    tab.addEventListener("click", changeTabs);
  }); // Enable arrow navigation between tabs in the tab list

  var tabFocus = 0;
  tabList.addEventListener("keydown", function (e) {
    // Move right
    if (e.keyCode === 39 || e.keyCode === 37) {
      tabs[tabFocus].setAttribute("tabindex", -1);

      if (e.keyCode === 39) {
        tabFocus++; // If we're at the end, go to the start

        if (tabFocus >= tabs.length) {
          tabFocus = 0;
        } // Move left

      } else if (e.keyCode === 37) {
        tabFocus--; // If we're at the start, move to the end

        if (tabFocus < 0) {
          tabFocus = tabs.length - 1;
        }
      }

      tabs[tabFocus].setAttribute("tabindex", 0);
      tabs[tabFocus].focus();
    }
  });
});

function changeTabs(e) {
  var target = e.target;
  var parent = target.parentNode;
  var grandparent = parent.parentNode; // Remove all current selected tabs

  parent.querySelectorAll('[aria-selected="true"]').forEach(function (t) {
    t.setAttribute("aria-selected", false);
    t.classList.remove("active");
  }); // Set this tab as selected

  target.setAttribute("aria-selected", true);
  target.classList.add("active"); // Hide all tab panels

  grandparent.querySelectorAll('[role="tabpanel"]').forEach(function (p) {
    return p.setAttribute("hidden", true);
  }); // Show the selected panel

  grandparent.parentNode.querySelector("#".concat(target.getAttribute("aria-controls"))).removeAttribute("hidden");
}
